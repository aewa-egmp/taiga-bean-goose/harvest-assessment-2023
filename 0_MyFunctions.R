# Mean and variance of n random variables
AvgMuVar.fcn = function(n,mu,var) {
  Mu = mean(mu)
  x = numeric(n)
  for (i in 1:n) x[i] = (mu[i]-Mu)^2
  Var = mean(var) + sum(x)/n
  answer = list(Mu,Var)
  names(answer)[[1]]="Mu"
  names(answer)[[2]]="Var"
  return(answer)
}

# Beta method of moments
MOM.beta=function(mu,var) {
  sum_ab=(mu*(1-mu)/var)-1
  a=sum_ab*mu
  b=sum_ab*(1-mu)
  answer=list(a,b)
  names(answer)[[1]]="a"
  names(answer)[[2]]="b"
  return(answer)
}

# Gamma MoM
MOM.gamma = function(mean,var) {
  shape = mean^2/var
  rate = mean/var
  answer=list(shape,rate)
  names(answer)[[1]]="shape"
  names(answer)[[2]]="rate"
  return(answer)
}

# Function to convert mean and sd to lognormal scale
lognorm.pr <- function(expect,stdev){
  prior.sig <- log(1+(stdev^2/expect^2))
  prior.se <- sqrt(prior.sig)
  prior.est <- log(expect)-(prior.sig*0.5)
  result <- c(prior.est,prior.se)
  return(result)
}

# Inverse logistic
expit = function(xx) { 1/(1 + exp(-xx)) }


ShowColors <- function(bg = "gray", cex = 0.75, srt = 30) {
  m <- ceiling(sqrt(n <- length(cl <- colors())))
  length(cl) <- m*m; cm <- matrix(cl, m)
  ##
  require("graphics")
  op <- par(mar=rep(0,4), ann=FALSE, bg = bg); on.exit(par(op))
  plot(1:m,1:m, type="n", axes=FALSE)
  text(col(cm), rev(row(cm)), cm,  col = cl, cex=cex, srt=srt)
}

# color blind colors
colb  <- c("#000000", "#E69F00", "#56B4E9", "#009E73", 
                       "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
colorblind = function() pie(rep(1, 8), col=colb)
#colorblind()
rgbcol = col2rgb(colb)/255
