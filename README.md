# EGMP Offtake Assessment for Finland and NW Russia / Sweden, Denmark and Germany population of Taiga Bean Goose (former Central MU) 


## Description
Data and model files for the 2023 EGMP offtake assessment for Finland and NW Russia / Sweden, Denmark and Germany population of Taiga Bean Goose (former Central MU).

## Year
2023

## Assessor
Fred Johnson, EGMP Data Centre

## How to cite this material
https://doi.org/10.5281/zenodo.8112860    

## Data
Data used in this assessment is available from the EGMP database (https://calm-dune-07f6d4603.azurestaticapps.net/ ), and can be used under the conditions stated in the EGMP Database Policy. Descriptions of the raw datasets are likewise available from the website.

