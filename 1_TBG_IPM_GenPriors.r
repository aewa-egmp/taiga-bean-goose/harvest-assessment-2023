# GenParms.r

rm(list=ls())
setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\B Taiga Bean Geese\\2023 Assessment\\Analysis')
source('0_MyFunctions.R')

#library(triangle)
library(rriskDistributions)
library(truncnorm)
library(mvtnorm)

# survival functions, Niel & Lebreton Eq. 15, and F function
s.fcn = function(mass,p,alpha) p^(1/(exp(3.22+0.24*log(mass)+rnorm(z,0,sqrt(0.087)))-alpha))
getlam1=function(a,s) { ((s*a-s+a+1)+sqrt((s-s*a-a-1)^2-4*s*a^2))/(2*a) }

###########################################################################################################
###########################################################################################################
z = 100000 # no. of samples

# Survival calculations
alpha = 3 # age at first breeding
mass.mu = c(2.843,3.198) # female, male mass (Dunning 2008)
mass.var = c(0.274,0.302)
mass.par = AvgMuVar.fcn(2,mass.mu,mass.var)

# ... replicates of body mass
s.parm = MOM.gamma(mass.par$Mu,mass.par$Var)
mass = rgamma(z,shape=s.parm[[1]],rate=s.parm[[2]]) # generate mass replicates

# ... replicates of proportion alive at max longevity (Johnson et al. 2012)
p = rbeta(z,3.34,101.24)

# ... replicates of survival (Slade et al. 1998)
s = s.fcn(mass,p,alpha)
mean(s);sd(s)
quantile(s,probs=c(0.05,0.50,0.95))
p = seq(0,1,0.2)
Psi.q = quantile(s,probs = p)
(spar = get.beta.par(p,Psi.q))
hist(s,freq = F,main='Natural survival rate');box()
curve(dbeta(x,spar[1],spar[2]),add=TRUE,col='red',lwd=2)
logit.s = log(s/(1-s))
hist(logit.s,freq=FALSE)
curve(dnorm(x,mean(logit.s),sd(logit.s)),0,5,add=TRUE)
get.norm.par(p,quantile(logit.s,p),plot=FALSE)

# lam1 is the analytical solution for Niel & Lebreton equation 15, given age at first breeding (a) and adult survival (s)
lam = getlam1(3,s)
summary(lam)

# get gamma (repro rate)
gamma.fcn = function(s,lam) lam/s - 1
gamma = NULL
for (i in 1:z) gamma[i] = gamma.fcn(s[i],lam[i])
mean(gamma); sd(gamma)
log.gamma = log(gamma)
hist(log.gamma,freq=FALSE,breaks=50)
curve(dnorm(x,mean(log.gamma),sd(log.gamma)),-2,1,add=TRUE)
gammaq = quantile(gamma,probs=p)
gamma.par = get.lnorm.par(p=p,q=gammaq,plot=FALSE)

sg = cbind(logit.s,log.gamma)  # for multivariate normal
(mu = c(mean(sg[,1]),mean(sg[,2])))
(sigma = cov(sg))

# generate thetas (Saether and Engen 2002)
r = log(lam)
mean(r)
quantile(r,probs=c(0.05,0.50,0.95))
e1=rnorm(z,0,sqrt(0.9418))
theta1 = exp(1.1286-1.8244*r+e1)
mean(theta1);sd(theta1)
# use truncated normal for error term
e = rtruncnorm(z,a=-1.5,b=1.5,0,sqrt(0.9418))
theta = exp(1.1286-1.8244*r+e)
summary(theta1);
mean(theta);sd(theta)
#summary(r*theta/(theta+1)) # hMSY
theta.q = quantile(theta,p)
tpar = get.lnorm.par(p,theta.q,plot=FALSE)
hist(theta,freq=F,breaks=20,main='Form of density dependence');box()
curve(dlnorm(x,tpar[1],tpar[2]),0,12,add=T,col='red',lwd=2)
curve(dlnorm(x,tpar[1],tpar[2]/2),0,12,add=T,col='blue',lwd=2)
qlnorm(p=c(0.025,0.5,0.975),tpar[1],tpar[2]/2)

# Carrying capacity
#... breeding ground
curN = 41800
(n=c(curN*1.75,curN*2,curN*2.25))
(par=get.lnorm.par(p=c(0.05,0.50,0.95),q=n,tol=0.001))
#z=100000
k = rlnorm(z,par[1],par[2])
mean(k)
quantile(k,probs=c(0.025,0.50,0.975))

# Germany from Heinicke 2013 ppt
DE = data.frame(year=c(2004,2005,2006,2009,2011,2013,2015),jande=c(35000,42000,52000,22500,12100,12800,9600))
#parDE = lognorm.pr(mean(DE$jande[4:7]),sd(DE$jande[4:7]))
parDE = lognorm.pr(mean(DE$jande[]),sd(DE$jande[]))

